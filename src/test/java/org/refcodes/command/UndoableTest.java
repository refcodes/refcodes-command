// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.command;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import org.junit.jupiter.api.Test;

public class UndoableTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testUnduable() throws Exception {
		// Reference list to compare the result:
		final List<Integer> theRef = createReferenceList();
		// The undo Stack to use: 
		final Deque<TestCommand> theStack = new ArrayDeque<>();
		// The actual context to work on:
		final List<Integer> theCtx = createReferenceList();
		// The commands to be applied to the context:
		final TestCommand incBy2 = new AddCommand( 2 );
		final TestCommand incBy4 = new AddCommand( 4 );
		final TestCommand incBy8 = new AddCommand( 8 );
		final TestCommand decBy1 = new SubCommand( 1 );
		final TestCommand decBy2 = new SubCommand( 2 );
		final TestCommand decBy3 = new SubCommand( 3 );
		final TestCommand mulBy2 = new MulCommandImpl( 2 );
		final TestCommand mulBy5 = new MulCommandImpl( 5 );
		// Apply the commands:
		execute( incBy2, theCtx, theStack );
		assertNotEquals( theCtx, theRef );
		execute( incBy4, theCtx, theStack );
		assertNotEquals( theCtx, theRef );
		execute( incBy8, theCtx, theStack );
		assertNotEquals( theCtx, theRef );
		execute( mulBy2, theCtx, theStack );
		assertNotEquals( theCtx, theRef );
		execute( decBy1, theCtx, theStack );
		assertNotEquals( theCtx, theRef );
		execute( decBy2, theCtx, theStack );
		assertNotEquals( theCtx, theRef );
		execute( decBy3, theCtx, theStack );
		assertNotEquals( theCtx, theRef );
		execute( mulBy5, theCtx, theStack );
		assertNotEquals( theCtx, theRef );
		// Undo all operations in reverse order:
		final Iterator<TestCommand> e = theStack.iterator();
		while ( e.hasNext() ) {
			e.next().undo( theCtx );
			e.remove();
			if ( e.hasNext() ) {
				assertNotEquals( theCtx, theRef );
			}
		}
		assertEquals( theCtx, theRef );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void execute( TestCommand aCommand, List<Integer> aCtx, Deque<TestCommand> aStack ) throws Exception {
		aCommand.execute( aCtx );
		aStack.addFirst( aCommand );
	}

	private List<Integer> createReferenceList() {
		final List<Integer> theList = new ArrayList<>();
		for ( int i = 1; i < 100; i++ ) {
			theList.add( i * 1000 );
		}
		return theList;
	}
}
