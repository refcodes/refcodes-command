// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.command;

import java.util.List;

public class MulCommandImpl implements TestCommand {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final int _multiplier;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new mul command impl.
	 *
	 * @param aMultiplier the multiplier
	 */
	public MulCommandImpl( int aMultiplier ) {
		_multiplier = aMultiplier;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Integer> execute( List<Integer> aContext ) {
		int eElement;
		for ( int i = 0; i < aContext.size(); i++ ) {
			eElement = aContext.remove( i ) * _multiplier;
			aContext.add( i, eElement );
		}
		return aContext;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isUndoable( List<Integer> aContext ) {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void undo( List<Integer> aContext ) {
		int eElement;
		for ( int i = 0; i < aContext.size(); i++ ) {
			eElement = aContext.remove( i ) / _multiplier;
			aContext.add( i, eElement );
		}
	}
}
