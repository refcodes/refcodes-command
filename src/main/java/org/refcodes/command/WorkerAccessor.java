// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.command;

/**
 * Provides an accessor for a command property.
 *
 * @param <CTX> the generic type
 * @param <E> the element type
 */
public interface WorkerAccessor<CTX, E extends Exception> {

	/**
	 * Retrieves the worker from the worker property.
	 * 
	 * @return The worker stored by the worker property.
	 */
	Worker<CTX, E> getWorker();

	/**
	 * Provides a mutator for a worker property.
	 *
	 * @param <CTX> the generic type
	 * @param <E> the element type
	 */
	public interface WorkerMutator<CTX, E extends Exception> {

		/**
		 * Sets the worker for the worker property.
		 * 
		 * @param aCommand The worker to be stored by the worker property.
		 */
		void setWorker( Worker<CTX, E> aCommand );
	}

	/**
	 * Provides a builder method for a name property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <CTX> the generic type
	 * @param <E> the element type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface WorkerBuilder<CTX, E extends Exception, B extends WorkerBuilder<CTX, E, B>> {

		/**
		 * Sets the worker for the worker property.
		 * 
		 * @param aCommand The worker to be stored by the worker property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withWorker( Worker<CTX, E> aCommand );
	}

	/**
	 * Provides a worker property.
	 *
	 * @param <CTX> the generic type
	 * @param <E> the element type
	 */
	public interface WorkerProperty<CTX, E extends Exception> extends WorkerAccessor<CTX, E>, WorkerMutator<CTX, E> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Worker} (setter)
		 * as of {@link #setWorker(Worker)} and returns the very same value
		 * (getter).
		 * 
		 * @param aWorker The {@link Worker} to set (via
		 *        {@link #setWorker(Worker)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Worker<CTX, E> letWorker( Worker<CTX, E> aWorker ) {
			setWorker( aWorker );
			return aWorker;
		}
	}
}
