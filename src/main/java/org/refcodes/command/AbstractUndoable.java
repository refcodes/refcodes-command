// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.command;

import java.awt.Component;

/**
 * The {@link AbstractUndoable} implements the {@link Undoable} interface.
 *
 * @param <CTX> The context type to use, can by any {@link Component}, service
 *        or POJO.
 * @param <RET> The return type of the {@link Undoable}'s proceedings.
 * @param <E> the element type
 */
public abstract class AbstractUndoable<CTX, RET, E extends Exception> extends AbstractCommand<CTX, RET, E> implements Undoable<CTX, RET, E> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isUndoable( CTX aContext ) {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void undo( CTX aContext ) {
		throw new UnsupportedOperationException( "This command is not undoable." );
	}
}
