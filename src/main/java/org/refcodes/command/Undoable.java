// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.command;

import java.awt.Component;

/**
 * An {@link Undoable} is a {@link Command}) which also provides means to undo
 * its operation applied before. The {@link Undoable} is created by a client
 * (e.g. the business logic) and passed to something like a command-bus or
 * command-processor for execution.
 *
 * @param <CTX> The context type to use, can by any {@link Component}, service
 *        or POJO.
 * @param <RET> The return type of the {@link Undoable}'s proceedings.
 * @param <E> The exception type of the {@link Undoable}'s erroneous
 *        termination.
 */
public interface Undoable<CTX, RET, E extends Exception> extends Command<CTX, RET, E> {

	/**
	 * This method determines whether the {@link Undoable} can undo the work it
	 * has done.
	 * 
	 * @param aContext The target object which is used by the {@link Undoable}
	 *        to do its {@link Undoable}.
	 * 
	 * @return True if undoable.
	 */
	boolean isUndoable( CTX aContext );

	/**
	 * This method tries to undo the {@link Undoable} in case it was
	 * successfully executed. Test via {@link #isUndoable(Object)} beforehand to
	 * see whether a call to {@link #undo(Object)} can be applied.
	 * 
	 * @param aContext The target object which is used by the {@link Undoable}
	 *        to do its {@link Undoable}.
	 * 
	 * @throws NotUndoableRuntimeException Thrown in case the performed
	 *         operation cannot be undone, or no operation as been done before
	 *         by this {@link Undoable}.
	 * @throws UnsupportedOperationException Thrown in case the {@link Undoable}
	 *         does not provide undo facilities.
	 */
	void undo( CTX aContext );

}
