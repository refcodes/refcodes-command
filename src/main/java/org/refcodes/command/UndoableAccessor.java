// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.command;

/**
 * Provides an accessor for a command property.
 *
 * @param <CTX> the generic type
 * @param <R> the generic type
 * @param <E> the element type
 */
public interface UndoableAccessor<CTX, R, E extends Exception> {

	/**
	 * Retrieves the undoable from the undoable property.
	 * 
	 * @return The undoable stored by the undoable property.
	 */
	Undoable<CTX, R, E> getUndoable();

	/**
	 * Provides a mutator for a undoable property.
	 *
	 * @param <CTX> the generic type
	 * @param <R> the generic type
	 * @param <E> the element type
	 */
	public interface UndoableMutator<CTX, R, E extends Exception> {

		/**
		 * Sets the undoable for the undoable property.
		 * 
		 * @param aCommand The undoable to be stored by the undoable property.
		 */
		void setUndoable( Undoable<CTX, R, E> aCommand );
	}

	/**
	 * Provides a builder method for a name property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <CTX> the generic type
	 * @param <R> the generic type
	 * @param <E> the element type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface UndoableBuilder<CTX, R, E extends Exception, B extends UndoableBuilder<CTX, R, E, B>> {

		/**
		 * Sets the undoable for the undoable property.
		 * 
		 * @param aCommand The undoable to be stored by the undoable property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withUndoable( Undoable<CTX, R, E> aCommand );
	}

	/**
	 * Provides a undoable property.
	 *
	 * @param <CTX> the generic type
	 * @param <R> the generic type
	 * @param <E> the element type
	 */
	public interface UndoableProperty<CTX, R, E extends Exception> extends UndoableAccessor<CTX, R, E>, UndoableMutator<CTX, R, E> {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Undoable}
		 * (setter) as of {@link #setUndoable(Undoable)} and returns the very
		 * same value (getter).
		 * 
		 * @param aUndoable The {@link Undoable} to set (via
		 *        {@link #setUndoable(Undoable)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Undoable<CTX, R, E> letUndoable( Undoable<CTX, R, E> aUndoable ) {
			setUndoable( aUndoable );
			return aUndoable;
		}
	}
}
