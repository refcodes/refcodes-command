// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.command;

/**
 * Provides an accessor for a command property.
 *
 * @param <CTX> the generic type
 * @param <R> the generic type
 * @param <E> the element type
 */
public interface CommandAccessor<CTX, R, E extends Exception> {

	/**
	 * Retrieves the command from the command property.
	 * 
	 * @return The command stored by the command property.
	 */
	Command<CTX, R, E> getCommand();

	/**
	 * Provides a mutator for a command property.
	 *
	 * @param <CTX> the generic type
	 * @param <R> the generic type
	 * @param <E> the element type
	 */
	public interface CommandMutator<CTX, R, E extends Exception> {

		/**
		 * Sets the command for the command property.
		 * 
		 * @param aCommand The command to be stored by the command property.
		 */
		void setCommand( Command<CTX, R, E> aCommand );
	}

	/**
	 * Provides a builder method for a name property returning the builder for
	 * applying multiple build operations.
	 *
	 * @param <CTX> the generic type
	 * @param <R> the generic type
	 * @param <E> the element type
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface CommandBuilder<CTX, R, E extends Exception, B extends CommandBuilder<CTX, R, E, B>> {

		/**
		 * Sets the command for the command property.
		 * 
		 * @param aCommand The command to be stored by the command property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withCommand( Command<CTX, R, E> aCommand );
	}

	/**
	 * Provides a command property.
	 *
	 * @param <CTX> the generic type
	 * @param <R> the generic type
	 * @param <E> the element type
	 */
	public interface CommandProperty<CTX, R, E extends Exception> extends CommandAccessor<CTX, R, E>, CommandMutator<CTX, R, E> {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Command} (setter)
		 * as of {@link #setCommand(Command)} and returns the very same value
		 * (getter).
		 * 
		 * @param aCommand The {@link Command} to set (via
		 *        {@link #setCommand(Command)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Command<CTX, R, E> letCommand( Command<CTX, R, E> aCommand ) {
			setCommand( aCommand );
			return aCommand;
		}
	}
}
