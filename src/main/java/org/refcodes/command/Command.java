// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.command;

import java.awt.Component;

/**
 * A {@link Command} ({@link Command}) represents an (atomic) operation applied
 * to a context returning a result encapsulated in an object (as of object
 * oriented programming). An {@link Undoable} is a command which provides means
 * to undo its operation applied before. The {@link Command} is created by a
 * client (e.g. the business logic) and passed to something like a command-bus
 * or command-processor for execution.
 *
 * @param <CTX> The context type to use, can by any {@link Component}, service
 *        or POJO.
 * @param <RET> The return type of the {@link Command}'s proceedings.
 * @param <E> The exception type of the {@link Command}'s erroneous termination.
 */
@FunctionalInterface
public interface Command<CTX, RET, E extends Exception> /* extends Resetable */ {

	/**
	 * The invoker executes a {@link Command} by providing it a context (being a
	 * service, a {@link Component} or a POJO). The method works synchronously
	 * and waits (blocks the caller's thread) till a result is available.
	 *
	 * @param aContext The target object (being a service, a {@link Component}
	 *        or a POJO) which is used by the {@link Command} to perform its
	 *        (atomic) operation.
	 * 
	 * @return The result of this {@link Command} after execution or Void in
	 *         case this {@link Command} does not process a result.
	 * 
	 * @throws E the e
	 */
	RET execute( CTX aContext ) throws E;

	/**
	 * The invoker executes a {@link Command} by providing it a context (being a
	 * service, a {@link Component} or a POJO). The method works asynchronously;
	 * use
	 * 
	 * @param aContext The target object (being a service, a {@link Component}
	 *        or a POJO) which is used by the {@link Command} to perform its
	 *        (atomic) operation.
	 */
	// public void start( CTX aContext ); // throws E;

	/**
	 * After execution, the {@link Command} might provide a result. This method
	 * determines whether the {@link Command} can provide a result. In case we
	 * have a void {@link Command} then after execution it will not provide a
	 * result.
	 * 
	 * @return True in case the {@link Command} can provide a result, else
	 *         false.
	 */
	// boolean hasResult();

	/**
	 * In case the {@link Command} can provide a result after execution, it can
	 * be retrieved with this method. In case it is a {@link Void}
	 * {@link Command} without a result, then an instace of the {@link Void}
	 * class should be returned. Use the {@link #hasResult()} method to test
	 * before calling this method. This method waits (halts the thread) until a
	 * result is available. In case you want to avoid synchronously waiting for
	 * the result, call the method {@link #hasResult()}.
	 * 
	 * @return The result of this {@link Command} after execution.
	 * 
	 * @throws UnsupportedOperationException in case the {@link Command} is a
	 *         void {@link Command} not returning any result.
	 * @throws IllegalStateException Thrown in case the {@link Command} has not
	 *         been executed yet.
	 */
	// RET getResult() throws E;
}
