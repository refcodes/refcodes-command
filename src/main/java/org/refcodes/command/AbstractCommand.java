// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.command;

import java.awt.Component;

/**
 * The {@link AbstractCommand} implements the {@link Undoable} interface.
 *
 * @param <CTX> The context type to use, can by any {@link Component}, service
 *        or POJO.
 * @param <RET> The return type of the {@link Undoable}'s proceedings.
 * @param <E> the element type
 */
public abstract class AbstractCommand<CTX, RET, E extends Exception> implements Command<CTX, RET, E> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	//	private RET _result = null;
	//	private E _exception = null;
	//	private boolean _hasResult = false;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	//	@Override
	//	public RET getResult() throws E {
	//
	//		if ( !_hasResult ) {
	//			synchronized ( this ) {
	//				if ( !_hasResult ) {
	//					try {
	//						this.wait();
	//					}
	//					catch ( InterruptedException ignored ) {}
	//				}
	//			}
	//		}
	//
	//		if ( (_result != null) && (_exception == null) ) {
	//			return _result;
	//		}
	//		if ( (_result == null) && (_exception != null) ) {
	//			throw _exception;
	//		}
	//		throw new IllegalStateException( "The combination of the result <" + _result + "> and exception the exception <" + _exception + "> being set represents an illegal state" );
	//	}

	//	@Override
	//	public RET execute( CTX aContext ) throws E {
	//		start( aContext );
	//		return getResult();
	//	}

	//	@Override
	//	public boolean hasResult() {
	//		return _hasResult;
	//	}

	//	@Override
	//	public void reset() {
	//		synchronized ( this ) {
	//			notifyAll();
	//		}
	//		_hasResult = false;
	//		_result = null;
	//		_exception = null;
	//	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Sets the result for the job. Any waiting threads via the
	 * {@link #getResult()} or {@link AbstractCommand#execute(Object)} method
	 * are notified and continue execution.
	 * 
	 * @param aResult The result to be set.
	 */
	//	protected void setResult( RET aResult ) {
	//		_result = aResult;
	//		_hasResult = true;
	//		synchronized ( this ) {
	//			this.notifyAll();
	//		}
	//	}

	/**
	 * Sets the exception for the job. Any waiting threads via the
	 * {@link #getResult()} or {@link AbstractCommand#execute(Object)} method
	 * are notified and continue execution.
	 * 
	 * @param aExceptiont The exception to be set.
	 */
	//	protected void setException( E aExceptiont ) {
	//		_exception = aExceptiont;
	//		_hasResult = true;
	//		synchronized ( this ) {
	//			this.notifyAll();
	//		}
	//	}
}
