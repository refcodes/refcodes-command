// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.command;

import org.refcodes.exception.AbstractRuntimeException;

/**
 * The Class {@link CommandCommandRuntimeException}.
 */
public abstract class CommandRuntimeException extends AbstractRuntimeException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public CommandRuntimeException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public CommandRuntimeException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public CommandRuntimeException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public CommandRuntimeException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public CommandRuntimeException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public CommandRuntimeException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * The Class {@link CommandCommandRuntimeException}.
	 */
	@SuppressWarnings("rawtypes")
	protected abstract static class CommandCommandRuntimeException extends CommandRuntimeException implements UndoableAccessor {

		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////////

		protected Undoable _command;

		// /////////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 * 
		 * @param aCommand The command involved in this exception.
		 */
		public CommandCommandRuntimeException( String aMessage, Undoable aCommand, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_command = aCommand;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aCommand The command involved in this exception.
		 */
		public CommandCommandRuntimeException( String aMessage, Undoable aCommand, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_command = aCommand;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aCommand The command involved in this exception.
		 */
		public CommandCommandRuntimeException( String aMessage, Undoable aCommand, Throwable aCause ) {
			super( aMessage, aCause );
			_command = aCommand;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aCommand The command involved in this exception.
		 */
		public CommandCommandRuntimeException( String aMessage, Undoable aCommand ) {
			super( aMessage );
			_command = aCommand;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aCommand The command involved in this exception.
		 */
		public CommandCommandRuntimeException( Undoable aCommand, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_command = aCommand;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aCommand The command involved in this exception.
		 */
		public CommandCommandRuntimeException( Undoable aCommand, Throwable aCause ) {
			super( aCause );
			_command = aCommand;
		}

		// /////////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Undoable getUndoable() {
			return _command;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getPatternArguments() {
			return new Object[] { _command };
		}
	}
}
