// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.command;

import java.awt.Component;

/**
 * A {@link Worker} represents an (atomic) operation applied to a context and
 * (in contrast to a {@link Command}) not returning any result, all of which
 * encapsulated in an object (as of object oriented programming). A
 * {@link Worker} is created by a client (e.g. the business logic) and passed to
 * something like a command-bus or command-processor for execution.
 *
 * @param <CTX> The context type to use, can by any {@link Component}, service
 *        or POJO.
 * @param <E> The exception type of the {@link Worker}'s erroneous termination.
 */
@FunctionalInterface
public interface Worker<CTX, E extends Exception> { // extends Command<CTX,
													 // Void, E> {

	/**
	 * The invoker executes a {@link Worker} by providing it a context (being a
	 * service, a {@link Component} or a POJO). The method works synchronously
	 * and waits (blocks the caller's thread) till it is finished.
	 *
	 * @param aContext The target object (being a service, a {@link Component}
	 *        or a POJO) which is used by the {@link Worker} to perform its
	 *        (atomic) operation.
	 * 
	 * @throws E the e
	 */
	void execute( CTX aContext ) throws E;
}
