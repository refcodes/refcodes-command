module org.refcodes.command {
	requires transitive org.refcodes.exception;
	requires java.desktop;

	exports org.refcodes.command;
}
